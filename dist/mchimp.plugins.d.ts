import 'typings-global';
import * as smartq from 'smartq';
import * as smartrequest from 'smartrequest';
import * as smartstring from 'smartstring';
import * as tsclass from 'tsclass';
export { smartq, smartrequest, smartstring, tsclass };
