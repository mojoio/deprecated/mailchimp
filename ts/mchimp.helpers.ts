import * as plugins from './mchimp.plugins'

export let getDataCenterFromKey = (keyArg: string): string => {
  let matchingRegex = /\-(.*)/
  let datacenter = matchingRegex.exec(keyArg)[1]
  return datacenter
}
